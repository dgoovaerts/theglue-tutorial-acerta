package com.theglue.core.journey.tutorial.manageaccountbalance;


import javax.inject.Named;

import com.theglue.core.journey.tutorial.manageaccountbalance.data.journeydata.ManageAccountBalanceJourneyData;

import com.theglue.api.events.UnauthorizedAbac;


import com.theglue.core.journey.tutorial.manageaccountbalance.events.AccountBalanceRequested;


import com.theglue.core.journey.tutorial.manageaccountbalance.process.business.RetrieveAccountBalance;


import com.theglue.api.processing.builders.business.BusinessJourney;
import com.theglue.api.processing.builders.business.BusinessJourneyBuilder;

@Named
public class ManageAccountBalanceJourney implements BusinessJourney {

    @Override
    public void configure(BusinessJourneyBuilder journey) {
        journey
                 .withData(ManageAccountBalanceJourneyDataDefinition.class)
                 .event(AccountBalanceRequested.class).isInitiating().action(RetrieveAccountBalance.class).require(new AccountMandateCheck()).end().end();
    }
}
