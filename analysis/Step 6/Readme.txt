Adaptation to 
 - Manage Account Balance journey: 
	- ABAC check
	- Parameters for EAC checks
	- Journey DSL to activate ABAC check

+ 

Setup of context enricher:

	Query 
		FROM : journey data "Role Context Enricher"
		SELECT : role
		WHERE : <role-context-enricher>.userIdentifier = <HTTP Header>.userId AND
			<role-context-enricher>.status = "ACTIVE"

	

No impact on other journeys