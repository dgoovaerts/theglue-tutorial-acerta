Setup communication journey which :
	- Receives as input the "Entity Key" (= accountReference)
	- Key is sent out in a JSON to Thin-ESB
	- Thin-ESB answers with JSON matching exactly structure of "accountBalance" entity
	- Answer is put on "EntityFound" event