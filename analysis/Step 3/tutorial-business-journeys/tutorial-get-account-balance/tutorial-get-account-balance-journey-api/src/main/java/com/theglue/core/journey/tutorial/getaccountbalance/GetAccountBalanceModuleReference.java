package com.theglue.core.journey.tutorial.getaccountbalance;

import com.theglue.api.ModuleReference;

public class GetAccountBalanceModuleReference {

    public static final ModuleReference GET_ACCOUNT_BALANCE_JOURNEY = ModuleReference.create("core", "tutorial", "get-account-balance", "3.0");
}
