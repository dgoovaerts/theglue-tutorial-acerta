package com.theglue.core.journey.tutorial.getaccountbalance;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.theglue.core.journey.tutorial.getaccountbalance", "com.theglue.core.entity.tutorial.get-account-balance"})
public class BasicJourneySpringConfig {

}
