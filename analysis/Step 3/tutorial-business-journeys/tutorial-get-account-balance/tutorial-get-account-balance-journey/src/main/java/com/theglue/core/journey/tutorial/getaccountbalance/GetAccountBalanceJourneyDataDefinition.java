package com.theglue.core.journey.tutorial.getaccountbalance;

import javax.inject.Named;

import com.theglue.api.journeydata.JourneyDataDefinition;
import com.theglue.api.journeydata.builders.JourneyDataBuilder;
import com.theglue.core.journey.tutorial.getaccountbalance.data.journeydata.GetAccountBalanceJourneyData;
import com.theglue.core.journey.tutorial.getaccountbalance.data.journeydata.GetAccountBalanceJourneyData.query.getaccountbalancejourneydata.GetAccountBalanceJourneyDataQueryObject;

@Named
public class GetAccountBalanceJourneyDataDefinition implements JourneyDataDefinition<GetAccountBalanceJourneyData> {

    @Override
    public void define(JourneyDataBuilder<GetAccountBalanceJourneyData> journeyDataBuilder) {
        journeyDataBuilder
            .journeyDataOfType(GetAccountBalanceJourneyData.class)
            .queryObject(GetAccountBalanceJourneyDataQueryObject.class);
    }

}
