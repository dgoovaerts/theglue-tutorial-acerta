package com.theglue.core.journey.tutorial.getaccountbalance;

import com.theglue.api.processing.builders.communication.CommunicationJourneyBuilder;
import com.theglue.communication.journey.abstracthttp.HttpAbstractJourney;
import com.theglue.communication.journey.abstracthttp.event.HttpResultEvent;

import com.theglue.core.journey.tutorial.getaccountbalance.events.EntityRequestedByKey;
import com.theglue.core.journey.tutorial.getaccountbalance.events.HttpResultEvent;

import com.theglue.core.journey.tutorial.getaccountbalance.process.FetchByKeyProcessor;
import com.theglue.core.journey.tutorial.getaccountbalance.process.HttpResultEventProcessor;


import com.theglue.api.processing.Journey;
import com.theglue.api.processing.builders.JourneyBuilder;

@Named
public class GetAccountBalanceJourney extends HttpAbstractJourney {

    @Override
    public void configure((CommunicationJourneyBuilder journey) {
        journey
                 .event(EntityRequestedByKey.class).action(FetchByKeyProcessor.class).end().end()
                 .event(HttpResultEvent.class).action(HttpResultEventProcessor.class).end().end()
;
    }
}
