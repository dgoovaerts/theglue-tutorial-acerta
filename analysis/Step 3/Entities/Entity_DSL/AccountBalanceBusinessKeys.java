package com.theglue.core.entity.tutorial.accountbalance;

import com.theglue.api.entity.BusinessKey;

public enum AccountBalanceBusinessKeys implements BusinessKey {

    ACCOUNT_REFERENCE("accountReference");

    private String fieldName;

    AccountBalanceBusinessKeys(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }
}
