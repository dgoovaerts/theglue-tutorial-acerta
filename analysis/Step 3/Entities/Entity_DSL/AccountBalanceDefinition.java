package com.theglue.core.entity.tutorial.accountbalance;

import javax.inject.Named;

import com.theglue.api.entity.EntityDefinition;
import com.theglue.api.entity.builders.EntityBuilder;


import static com.theglue.core.journey.tutorial.getaccountbalance.GetAccountBalanceModuleReference.GET_ACCOUNT_BALANCE_JOURNEY;


@Named
public class AccountBalanceDefinition implements EntityDefinition<AccountBalance> {

    @Override
    public void define(EntityBuilder<AccountBalance> entityBuilder) {
        entityBuilder
            .fetchEntityWithCommunicationJourney(GET_ACCOUNT_BALANCE_JOURNEY)
            .entityOfType(AccountBalance.class);
    }
}
