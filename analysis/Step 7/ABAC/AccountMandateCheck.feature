Feature: Account Mandate Check
  ABAC function to check account mandate

  Scenario: Check mandate
    When RIGHT ( <InputEvent>.<EventContext>.userId, 2 ) != RIGHT ( <JourneyData>.accountReference, 2 )
	Then ABAC condition fails
	Else continue with the action execution