package com.theglue.core.journey.tutorial.mail;

import com.theglue.api.ModuleReference;

public class MailModuleReference {

    public static final ModuleReference MAIL_JOURNEY = ModuleReference.create("core", "tutorial", "mail", "1.0");
}
