package com.theglue.core.journey.tutorial.mail;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.theglue.core.journey.tutorial.mail", "com.theglue.core.entity.tutorial.mail"})
public class BasicJourneySpringConfig {

}
