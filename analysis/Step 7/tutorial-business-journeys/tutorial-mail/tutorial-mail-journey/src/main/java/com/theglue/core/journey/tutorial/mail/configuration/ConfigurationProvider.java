package com.theglue.core.journey.tutorial.mail.configuration;

import com.theglue.api.configuration.ConfigurationService;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class ConfigurationProvider {

    @Inject
    private ConfigurationService configuration;

    public ConfigurationProvider(ConfigurationService configuration) {
        this.configuration = configuration;
    }

    public String getDefaultVersion() {
        return configuration.getString("defaultVersion");
    }

    public String getMailgun.api-key() {
        return configuration.getString("mailgun.api-key"));
    }

    public String getMailgun.url() {
        return configuration.getString("mailgun.url"));
    }

    public String getMailgun.from-address() {
        return configuration.getString("mailgun.from-address"));
    }

    public String getDestination-address() {
        return configuration.getString("destination-address"));
    }



}
