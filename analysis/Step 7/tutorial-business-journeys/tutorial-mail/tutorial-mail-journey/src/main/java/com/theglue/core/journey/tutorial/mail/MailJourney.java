package com.theglue.core.journey.tutorial.mail;

import com.theglue.api.processing.builders.communication.CommunicationJourneyBuilder;
import com.theglue.communication.journey.abstracthttp.HttpAbstractJourney;
import com.theglue.communication.journey.abstracthttp.event.HttpResultEvent;

import com.theglue.core.journey.tutorial.mail.events.SendMailRequested;
import com.theglue.api.events.ClientErrorOccurred;
import com.theglue.api.events.ServerErrorOccurred;

import com.theglue.core.journey.tutorial.mail.process.SendMail;
import com.theglue.core.journey.tutorial.mail.process.ProcessClientServerError;


import com.theglue.api.processing.Journey;
import com.theglue.api.processing.builders.JourneyBuilder;

@Named
public class MailJourney extends HttpAbstractJourney {

    @Override
    public void configure((CommunicationJourneyBuilder journey) {
        journey
                 .event(SendMailRequested.class).isInitiating().action(SendMail.class).end().end()
                 .event(ClientErrorOccurred.class).action(ProcessClientServerErrorForClientErrorOccurred.class).end().end()
                 .event(ServerErrorOccurred.class).action(ProcessClientServerErrorForServerErrorOccurred.class).end().end()


                 //Final events
                 .event(MailSubmitted.class).isFinal()
                 .event(MailError.class).isFinal()
;
    }
}
