package com.theglue.core.journey.tutorial.credit;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.theglue.core.journey.tutorial.credit", "com.theglue.core.entity.tutorial.credit"})
public class BasicJourneySpringConfig {

}
