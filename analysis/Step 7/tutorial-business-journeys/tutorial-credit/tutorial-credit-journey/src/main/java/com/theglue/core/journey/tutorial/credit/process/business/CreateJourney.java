package com.theglue.core.journey.tutorial.credit.process.business;

import static com.theglue.api.processing.Result.result;

import javax.inject.Inject;
import javax.inject.Named;

import com.theglue.api.InjectTheGlueContext;
import com.theglue.api.TheGlueContext;
import com.theglue.api.processing.JourneyProcessor;
import com.theglue.api.processing.ProcessingContext;
import com.theglue.api.processing.Result;

import com.theglue.core.journey.tutorial.credit.data.journeydata.CreditJourneyData;
import com.theglue.core.journey.tutorial.credit.events.CreationRequested;
import com.theglue.core.journey.tutorial.credit.events.JourneyCreated;

@Named
public class CreateJourney implements JourneyProcessor<com.theglue.core.journey.tutorial.credit.events.CreationRequested, CreditJourneyData> {

    @InjectTheGlueContext
    private TheGlueContext theGlueContext;

    @Override
    public Result process(ProcessingContext<com.theglue.core.journey.tutorial.credit.events.CreationRequested, CreditJourneyData> processingContext) {
        CreditJourneyData journeyData = processingContext.getJourneyData();
        CreationRequested incomingEvent = processingContext.getEvent();

    	/** Introduce code of the action here
    	*/


	return Result.result().postFeedbackEvent(incomingEvent);

    }

    /** Introduce code of the action here
    */
}
