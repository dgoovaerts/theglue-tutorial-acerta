package com.theglue.core.journey.tutorial.credit;


import javax.inject.Named;

import com.theglue.core.journey.tutorial.credit.data.journeydata.CreditJourneyData;



import com.theglue.core.journey.tutorial.credit.events.CreationRequested;


import com.theglue.core.journey.tutorial.credit.process.business.CreateJourney;


import com.theglue.api.processing.builders.business.BusinessJourney;
import com.theglue.api.processing.builders.business.BusinessJourneyBuilder;

@Named
public class CreditJourney implements BusinessJourney {

    @Override
    public void configure(BusinessJourneyBuilder journey) {
        journey
                 .withData(CreditJourneyDataDefinition.class)
                 .event(CreationRequested.class).isInitiating().action(CreateJourney.class).end().end()
;
    }
}
