package com.theglue.core.journey.tutorial.credit;

import javax.inject.Named;

import com.theglue.api.journeydata.JourneyDataDefinition;
import com.theglue.api.journeydata.builders.JourneyDataBuilder;
import com.theglue.core.journey.tutorial.credit.data.journeydata.CreditJourneyData;
import com.theglue.core.journey.tutorial.credit.data.journeydata.CreditJourneyData.query.creditjourneydata.CreditJourneyDataQueryObject;

@Named
public class CreditJourneyDataDefinition implements JourneyDataDefinition<CreditJourneyData> {

    @Override
    public void define(JourneyDataBuilder<CreditJourneyData> journeyDataBuilder) {
        journeyDataBuilder
            .journeyDataOfType(CreditJourneyData.class)
            .queryObject(CreditJourneyDataQueryObject.class);
    }

}
