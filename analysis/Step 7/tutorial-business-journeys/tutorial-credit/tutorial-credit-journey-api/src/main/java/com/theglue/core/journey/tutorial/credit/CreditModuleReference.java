package com.theglue.core.journey.tutorial.credit;

import com.theglue.api.ModuleReference;

public class CreditModuleReference {

    public static final ModuleReference CREDIT_JOURNEY = ModuleReference.create("core", "tutorial", "credit", "1.0");
}
