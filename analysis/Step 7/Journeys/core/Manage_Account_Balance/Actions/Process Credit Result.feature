Feature: Process Credit Result
  Process credit result

  Scenario: Check input event
    Given Action started
	 When <InputEvent>.returnCode = "OK"
	 Then Create FEEDBACK output event "Account Balance Retrieved" with
		
			<OutputEventData>.returnCode = "OK"
			<OutputEventData>.returnMessage = "Account balance correctly retrieved (including credit started)"
			<OutputEventData>.actionResult.accountListWithBalance = <JourneyData>.accountListWithBalance
	
	 Else Create FEEDBACK output event "Account Balance Retrieved" with
		
			<OutputEventData>.returnCode = "OK"
			<OutputEventData>.returnMessage = "Account balance correctly retrieved (issue launching credit)"
			<OutputEventData>.actionResult.accountListWithBalance = <JourneyData>.accountListWithBalance