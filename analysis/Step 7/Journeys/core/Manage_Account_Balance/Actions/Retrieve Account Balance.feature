Feature: Retrieve Account Balance
  Retrieve Account Balance

  Scenario: Check number of input accounts
    Given Action started
	 When <InputEvent>.accountList contains more than 1 entry
	 Then Create FEEDBACK output event "Account Balance Error" with
	 
			<OutputEventData>.returnCode = "F201"
			<OutputEventData>.returnMessage = "Only 1 account can be provided as input"
			
      And Exit Action

  Scenario: Retrieve Account Balance
    Given Scenario "Check number of input accounts" completed
	 When value of configuration parameter "core#tutorial#manage-account-balance#accountBalanceSource" = "account"
	 Then Query (QueryResult)
			FROM : "account" journey
			SELECT : 
					- accountReference
					- balanceAmount
			WHERE :  account.accountReference = <InputEvent>.accountList.accountReference
	 
	 Else Fetch (QueryResult)
			FROM : "accountBalance" entity
			SELECT : 
					- accountReference
					- balanceAmount
			WHERE :  accountBalance.accountReference = <InputEvent>.accountList.accountReference
			
				with UBF = value of configuration parameter "core#tutorial#manage-account-balance#ubfAccountBalance" (if data not present in entity, automatic retrieval of data in back-end)

     When <QueryResult> != 1 record
	 Then Create FEEDBACK output event "Account Balance Error" with
	 
			<OutputEventData>.returnCode = "F202"
			<OutputEventData>.returnMessage = "Account not found"
			
      And Exit Action
			
  Scenario: Fill Journey Data
    Given Scenario "Retrieve Account Balance" completed
     Then Create new Journey
      And Fill journey data with details of input event, i.e.

		<JourneyData>.accountListWithBalance = create 1 entry for each entry in <InputEvent>.accountList array, with:
				<JourneyData>.accountListWithBalance.accountReference = <InputEvent>.accountList.accountReference
				<JourneyData>.accountListWithBalance.balanceAmount = <QueryResult>.balanceAmount
	 
  Scenario: Create output events
    Given Scenario "Fill Journey Data" completed
	 When <JourneyData>.accountListWithBalance.balanceAmount > value of configuration parameter "core#tutorial#manage-account-balance#thresholdExcessLiquidity"
	 Then Create output event "Send Mail Requested" of journey "Mail"
	 
	 When <JourneyData>.accountListWithBalance.balanceAmount < 0
	 Then Create output event "Creation Requested" of journey "Credit" with event data:
	 
		- <OutputEventData>.accountReference = <JourneyData>.accountListWithBalance.accountReference
		- <OutputEventData>.amount = Absolute value ( <JourneyData>.accountListWithBalance.balanceAmount ) + 500
      And Exit Scenario 

	 When <JourneyData>.accountListWithBalance.balanceAmount >= 0	  
	 Then finalizeJourney and Create FEEDBACK output event "Account Balance Retrieved" with
		
			<OutputEventData>.returnCode = "OK"
			<OutputEventData>.returnMessage = "Account balance correctly retrieved"
			<OutputEventData>.actionResult.accountListWithBalance = <JourneyData>.accountListWithBalance
			