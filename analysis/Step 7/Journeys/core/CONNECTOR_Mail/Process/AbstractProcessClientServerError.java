package com.theglue.core.journey.tutorial.mail.process;

import com.theglue.api.processing.JourneyId;
import com.theglue.api.processing.JourneyProcessor;
import com.theglue.api.processing.Result;
import com.theglue.commons.ObjectMapperHelper;
import com.theglue.communication.journey.abstracthttp.event.HttpEvent;

import com.theglue.core.journey.tutorial.mail.events.com.theglue.core.journey.tutorial.mail.events.Error;


import java.util.HashMap;
import java.util.Map;

import static com.theglue.api.processing.Result.result;



public class ProcessClientServerError implements JourneyProcessor<com.theglue.core.journey.tutorial.mail.events.Error, Void> {

	/** Introduce code of the action here
	*/
}
