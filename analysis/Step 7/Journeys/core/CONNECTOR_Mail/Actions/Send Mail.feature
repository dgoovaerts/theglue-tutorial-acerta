Feature: Connector for sending mail
  Connector for sending mail
  
   Scenario: Send Mail
	Given Scenario "Fill Template" Succeeded
	Then Send Mail by call via:

		public static ClientResponse SendSimpleMessage() {
			Client client = Client.create();
			client.addFilter(new HTTPBasicAuthFilter("api",
						"<Value of configuration parameter "core#tutorial#mail#mailgun.api-key">"));
			WebResource webResource =
				client.resource("<Value of configuration parameter "core#tutorial#mail#mailgun.url">");
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("from", "<Value of configuration parameter "core#tutorial#mail#mailgun.from-address">");
			formData.add("to", "<Value of configuration parameter "core#tutorial#mail#destination-address">");
			formData.add("subject", "Test Mail");
			formData.add("text", "Test Mail - Body");
			return webResource.type(MediaType.APPLICATION_FORM_URLENCODED).
														post(ClientResponse.class, formData);
		}

  Scenario: Generate output event
     Given Scenario "Send Mail" completed
	  When HTTP reply != 200 
	  Then Create output event "Mail Completed" with event data: 
		
		- <OutputEvent>.<HTTPStatus> = 400	  
		- <outputEvent>.returnCode = "NOK"
		- <outputEvent>.returnMessage = "Mail could not be sent"

	  Else Create output event "Mail Completed" with event data:
	 
		- <outputEvent>.returnCode = "OK"
		- <outputEvent>.returnMessage = "Mail successfully submitted"