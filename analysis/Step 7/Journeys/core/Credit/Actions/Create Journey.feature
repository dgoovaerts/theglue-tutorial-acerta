Feature: Create Journey
  Handle creation of credit

  Scenario: Enrich journey data
    Given Action started
     Then 
		<JourneyData>.accountReference = <InputEvent>.accountReference
		<JourneyData>.amount = <InputEvent>.amount
		
  Scenario: Create Output Events
    Given Scenario "Enrich journey data" completed
	 When <JourneyData>.amount > 5000 
	 Then Create FEEDBACK output event "Journey Created" with
		
			<OutputEventData>.returnCode = "F201"
			<OutputEventData>.returnMessage = "Credit refused"
			
	 Else Create FEEDBACK output event "Journey Created" with
		
			<OutputEventData>.returnCode = "OK"
			<OutputEventData>.returnMessage = "Credit successfully created"