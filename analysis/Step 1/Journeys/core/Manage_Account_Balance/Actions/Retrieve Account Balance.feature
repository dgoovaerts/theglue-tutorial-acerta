Feature: Retrieve Account Balance
  Retrieve Account Balance

  Scenario: Check number of input accounts
    Given Action started
	 When <InputEvent>.accountList contains more than 1 entry
	 Then Create FEEDBACK output event "Account Balance Error" with
	 
			<OutputEventData>.returnCode = "F201"
			<OutputEventData>.returnMessage = "Only 1 account can be provided as input"
			
      And Exit Action
	  
  Scenario: Fill Journey Data
    Given Scenario "Check number of input accounts" completed
     Then Create new Journey
     
      And Fill journey data with details of input event, i.e.

		<JourneyData>.accountListWithBalance = create 1 entry for each entry in <InputEvent>.accountList array, with:
				<JourneyData>.accountListWithBalance.accountReference = <InputEvent>.accountList.accountReference
				<JourneyData>.accountListWithBalance.balanceAmount = 50
	  
  Scenario: Create output event
     When Scenario "Fill Journey Data" completed
	 Then finalizeJourney and Create FEEDBACK output event "Account Balance Retrieved"  with
		
			<OutputEventData>.returnCode = "OK"
			<OutputEventData>.returnMessage = "Account balance correctly retrieved"
			<OutputEventData>.actionResult.accountListWithBalance = <JourneyData>.accountListWithBalance
			