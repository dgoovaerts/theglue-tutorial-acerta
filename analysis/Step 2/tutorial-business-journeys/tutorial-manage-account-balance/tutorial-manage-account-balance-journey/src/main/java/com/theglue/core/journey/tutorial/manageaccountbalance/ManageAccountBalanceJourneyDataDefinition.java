package com.theglue.core.journey.tutorial.manageaccountbalance;

import javax.inject.Named;

import com.theglue.api.journeydata.JourneyDataDefinition;
import com.theglue.api.journeydata.builders.JourneyDataBuilder;
import com.theglue.core.journey.tutorial.manageaccountbalance.data.journeydata.ManageAccountBalanceJourneyData;
import com.theglue.core.journey.tutorial.manageaccountbalance.data.journeydata.ManageAccountBalanceJourneyData.query.manageaccountbalancejourneydata.ManageAccountBalanceJourneyDataQueryObject;

@Named
public class ManageAccountBalanceJourneyDataDefinition implements JourneyDataDefinition<ManageAccountBalanceJourneyData> {

    @Override
    public void define(JourneyDataBuilder<ManageAccountBalanceJourneyData> journeyDataBuilder) {
        journeyDataBuilder
            .journeyDataOfType(ManageAccountBalanceJourneyData.class)
            .queryObject(ManageAccountBalanceJourneyDataQueryObject.class);
    }

}
