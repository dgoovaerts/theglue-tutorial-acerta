package com.theglue.core.journey.tutorial.manageaccountbalance;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.theglue.core.journey.tutorial.manageaccountbalance", "com.theglue.core.entity.tutorial.manage-account-balance"})
public class BasicJourneySpringConfig {

}
