package com.theglue.core.entity.tutorial.accountbalance;

import javax.inject.Named;

import com.theglue.api.entity.EntityDefinition;
import com.theglue.api.entity.builders.EntityBuilder;




@Named
public class AccountBalanceDefinition implements EntityDefinition<AccountBalance> {

    @Override
    public void define(EntityBuilder<AccountBalance> entityBuilder) {
        entityBuilder
            .withoutCommunicationJourney()
            .entityOfType(AccountBalance.class);
    }
}
