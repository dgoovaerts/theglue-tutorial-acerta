package com.theglue.core.journey.tutorial.rolecontextenricher.process.business;

import static com.theglue.api.processing.Result.result;

import javax.inject.Inject;
import javax.inject.Named;

import com.theglue.api.InjectTheGlueContext;
import com.theglue.api.TheGlueContext;
import com.theglue.api.processing.JourneyProcessor;
import com.theglue.api.processing.ProcessingContext;
import com.theglue.api.processing.Result;

import com.theglue.core.journey.tutorial.rolecontextenricher.data.journeydata.RoleContextEnricherJourneyData;
import com.theglue.core.journey.tutorial.rolecontextenricher.events.CreationRequested;
import com.theglue.core.journey.tutorial.rolecontextenricher.events.JourneyCreated;
import com.theglue.core.journey.tutorial.rolecontextenricher.events.RoleContextEnricherError;

@Named
public class CreateJourney implements JourneyProcessor<com.theglue.core.journey.tutorial.rolecontextenricher.events.CreationRequested, RoleContextEnricherJourneyData> {

    @InjectTheGlueContext
    private TheGlueContext theGlueContext;

    @Override
    public Result process(ProcessingContext<com.theglue.core.journey.tutorial.rolecontextenricher.events.CreationRequested, RoleContextEnricherJourneyData> processingContext) {
        RoleContextEnricherJourneyData journeyData = processingContext.getJourneyData();
        CreationRequested incomingEvent = processingContext.getEvent();

    	/** Introduce code of the action here
    	*/


	return Result.result().postFeedbackEvent(incomingEvent);

    }

    /** Introduce code of the action here
    */
}
