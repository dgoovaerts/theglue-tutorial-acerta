package com.theglue.core.journey.tutorial.rolecontextenricher;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.theglue.core.journey.tutorial.rolecontextenricher", "com.theglue.core.entity.tutorial.role-context-enricher"})
public class BasicJourneySpringConfig {

}
