package com.theglue.core.journey.tutorial.rolecontextenricher;

import javax.inject.Named;

import com.theglue.api.journeydata.JourneyDataDefinition;
import com.theglue.api.journeydata.builders.JourneyDataBuilder;
import com.theglue.core.journey.tutorial.rolecontextenricher.data.journeydata.RoleContextEnricherJourneyData;
import com.theglue.core.journey.tutorial.rolecontextenricher.data.journeydata.RoleContextEnricherJourneyData.query.rolecontextenricherjourneydata.RoleContextEnricherJourneyDataQueryObject;

@Named
public class RoleContextEnricherJourneyDataDefinition implements JourneyDataDefinition<RoleContextEnricherJourneyData> {

    @Override
    public void define(JourneyDataBuilder<RoleContextEnricherJourneyData> journeyDataBuilder) {
        journeyDataBuilder
            .journeyDataOfType(RoleContextEnricherJourneyData.class)
            .queryObject(RoleContextEnricherJourneyDataQueryObject.class);
    }

}
