package com.theglue.core.journey.tutorial.rolecontextenricher;

import com.theglue.api.ModuleReference;

public class RoleContextEnricherModuleReference {

    public static final ModuleReference ROLE_CONTEXT_ENRICHER_JOURNEY = ModuleReference.create("core", "tutorial", "role-context-enricher", "1.0");
}
