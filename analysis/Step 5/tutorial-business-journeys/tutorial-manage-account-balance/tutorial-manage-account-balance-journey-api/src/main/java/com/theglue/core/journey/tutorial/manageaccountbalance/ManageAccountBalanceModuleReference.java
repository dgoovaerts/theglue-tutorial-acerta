package com.theglue.core.journey.tutorial.manageaccountbalance;

import com.theglue.api.ModuleReference;

public class ManageAccountBalanceModuleReference {

    public static final ModuleReference MANAGE_ACCOUNT_BALANCE_JOURNEY = ModuleReference.create("core", "tutorial", "manage-account-balance", "5.0");
}
