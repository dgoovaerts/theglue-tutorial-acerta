package com.theglue.core.journey.tutorial.manageaccountbalance.configuration;

import com.theglue.api.configuration.ConfigurationService;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class ConfigurationProvider {

    @Inject
    private ConfigurationService configuration;

    public ConfigurationProvider(ConfigurationService configuration) {
        this.configuration = configuration;
    }

    public String getDefaultVersion() {
        return configuration.getString("defaultVersion");
    }

    public String getUbfAccountBalance() {
        return configuration.getString("ubfAccountBalance"));
    }

    public String getAccountBalanceSource() {
        return configuration.getString("accountBalanceSource"));
    }



}
