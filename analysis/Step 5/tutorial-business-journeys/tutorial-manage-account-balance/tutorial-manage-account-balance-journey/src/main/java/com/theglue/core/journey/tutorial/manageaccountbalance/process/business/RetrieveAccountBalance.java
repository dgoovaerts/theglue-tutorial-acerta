package com.theglue.core.journey.tutorial.manageaccountbalance.process.business;

import static com.theglue.api.processing.Result.result;

import javax.inject.Inject;
import javax.inject.Named;

import com.theglue.api.InjectTheGlueContext;
import com.theglue.api.TheGlueContext;
import com.theglue.api.processing.JourneyProcessor;
import com.theglue.api.processing.ProcessingContext;
import com.theglue.api.processing.Result;

import com.theglue.core.journey.tutorial.manageaccountbalance.data.journeydata.ManageAccountBalanceJourneyData;
import com.theglue.core.journey.tutorial.manageaccountbalance.events.AccountBalanceRequested;
import com.theglue.core.journey.tutorial.manageaccountbalance.events.AccountBalanceRetrieved;
import com.theglue.core.journey.tutorial.manageaccountbalance.events.AccountBalanceError;

@Named
public class RetrieveAccountBalance implements JourneyProcessor<com.theglue.core.journey.tutorial.manageaccountbalance.events.AccountBalanceRequested, ManageAccountBalanceJourneyData> {

    @InjectTheGlueContext
    private TheGlueContext theGlueContext;

    @Override
    public Result process(ProcessingContext<com.theglue.core.journey.tutorial.manageaccountbalance.events.AccountBalanceRequested, ManageAccountBalanceJourneyData> processingContext) {
        ManageAccountBalanceJourneyData journeyData = processingContext.getJourneyData();
        AccountBalanceRequested incomingEvent = processingContext.getEvent();

    	/** Introduce code of the action here
    	*/


	return Result.result().postFeedbackEvent(incomingEvent);

    }

    /** Introduce code of the action here
    */
}
