Feature: Process Modify
  Process Modify

  Scenario: Validate Input Data
	Given Action started
	 When <InputEvent>.role is not null AND <InputEvent>.role NOT IN value on configuration parameter "core#tutorial#role-context-enricher#input-validation-role"
	 Then Create FEEDBACK output event "Role Context Enricher Error" with output data:

				- <OutputEvent>.<HTTPStatusCode> = 400
				- <OutputEvent>.returnCode = "F101"
				- <OutputEvent>.returnMessage = "Invalid value for role"

	  And Exit Action

	 When <InputEvent>.status is not null AND <InputEvent>.status NOT IN value on configuration parameter "core#tutorial#role-context-enricher#input-validation-status"
	 Then Create FEEDBACK output event "Role Context Enricher Error" with output data:

				- <OutputEvent>.<HTTPStatusCode> = 400
				- <OutputEvent>.returnCode = "F102"
				- <OutputEvent>.returnMessage = "Invalid value for status"

	  And Exit Action
	  

  Scenario: Process Modification
    Given Scenario "Validate Input Data" completed
	 Then Update journey data:
	 
		 <JourneyData>.status = IF <InputEvent>.status is not null THEN <InputEvent>.status ELSE keep existing value
		 <JourneyData>.role = IF <InputEvent>.role is not null THEN <InputEvent>.role ELSE keep existing value

  Scenario: Create output event
     When Scenario "Process Modification" completed
	 Then Create FEEDBACK output event "Modification Completed" with event data:
	 
		- <OutputEvent>.returnCode = "OK"
		- <OutputEvent>.returnMessage = "Role Context Enricher successfully modified"