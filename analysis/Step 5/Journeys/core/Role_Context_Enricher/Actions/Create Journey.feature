Feature: Create Journey
  Create Journey
  
  Scenario: Validate Input Data
	Given Initiation of a Journey
	 When <InputEvent>.role NOT IN value on configuration parameter "core#tutorial#role-context-enricher#input-validation-role"
	 Then Create FEEDBACK output event "Role Context Enricher Error" with output data:

				- <OutputEvent>.<HTTPStatusCode> = 400
				- <OutputEvent>.returnCode = "F101"
				- <OutputEvent>.returnMessage = "Invalid value for role"

	  And Exit Action
	  
  Scenario: Check uniqueness of user
    Given Scenario "Validate Input Data" completed
	  And Query
			FROM : "role-context-enricher" journey
			SELECT : 	userIdentifier
			WHERE :  role-context-enricher.userIdentifier = <InputEvent>.userIdentifier

	 When <QueryResult> >= 1 record
     Then Create FEEDBACK output event "Role Context Enricher Error" with output data:
	 
				- <OutputEvent>.<HTTPStatusCode> = 400
				- <OutputEvent>.returnCode = "F201"
				- <OutputEvent>.returnMessage = "User identifier already existing"

	  And Exit Action
	  

  Scenario: Fill Journey Data
    Given Scenario "Check uniqueness of user" completed
     Then Create new Journey
     
      And Fill journey data with details of input event, i.e.
			
		 <JourneyData>.userIdentifier = <InputEvent>.userIdentifier
		 <JourneyData>.role = <InputEvent>.role
		 <JourneyData>.status = "ACTIVE"

		 
  Scenario: Create output event
     When Scenario "Fill Journey Data" completed
	 Then Create FEEDBACK output event "Journey Created" with event data:
	 
		- <OutputEvent>.returnCode = "OK"
		- <OutputEvent>.returnMessage = "Role Context Enricher successfully created"
