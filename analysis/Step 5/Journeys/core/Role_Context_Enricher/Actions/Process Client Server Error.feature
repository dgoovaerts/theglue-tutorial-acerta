Feature: Process Client Server Error
  Process Client Error or Server Error

  Scenario: Fill Feedback output event
    Given Action started
	
	 When <InputEventName> = "BeanValidationFailed"
	  And array "<InputEventData>.validationErrors" contains entry with "errorCode" = "NotNull"
	 Then Fill FEEDBACK output event "Role Context Enricher Error" with output data:
	
				- <OutputEvent>.<HTTPStatusCode> = 400
				- <OutputEvent>.returnCode = "F001"
				- <OutputEvent>.returnMessage = "Missing mandatory input value"
				- <OutputEvent>.errorResult = concatenation of <InputEvent>.validationErrors.attributeName and <InputEvent>.validationErrors.errorText (only validationErrorswith errorCode "NotNull")
	
	  And Exit Action
	  
	 When <InputEventName> = "BeanValidationFailed"
	 Then Create FEEDBACK output event "Role Context Enricher Error" with output data:

				- <OutputEvent>.<HTTPStatusCode> = 400
				- <OutputEvent>.returnCode = "F002"
				- <OutputEvent>.returnMessage = "Input validation failed"
				- <OutputEvent>.errorResult = concatenation of <InputEvent>.validationErrors.attributeName and <InputEvent>.validationErrors.errorText
				
	  And Exit Action
	  
	 When <InputEventName> IN ("UnauthorizedByExtensibleAccessControl", "UnauthorizedAbac", "PreconditionFailed")
	 Then Create FEEDBACK output event "Role Context Enricher Error" with output data:

				- <OutputEvent>.<HTTPStatusCode> = 400
				- <OutputEvent>.returnCode = "F090"
				- <OutputEvent>.returnMessage = "Operation not authorised"
				- <OutputEvent>.errorResult = <InputEventName>				
				
	  And Exit Action
	  
	 When <InputEvent> of type "ClientErrorOccurred"
	 Then Create FEEDBACK output event "Role Context Enricher Error" with output data:

				- <OutputEvent>.<HTTPStatusCode> = 400
				- <OutputEvent>.returnCode = "F099"
				- <OutputEvent>.returnMessage = "General client error"
				- <OutputEvent>.errorResult = <InputEventName> (i.e. EventNotFound, InitiatingExistingJourneyOccurred, InvalidRequestEvent, JourneyAlreadyFinalized or JourneyNotFound)								
				
	  And Exit Action
				
	 When <InputEventName> = "ServerErrorOccurred"
	 Then Create FEEDBACK output event "Role Context Enricher Error" with output data:

				- <OutputEvent>.<HTTPStatusCode> = 500
				- <OutputEvent>.returnCode = "T999"
				- <OutputEvent>.returnMessage = "General internal error"
				- <OutputEvent>.errorResult = <InputEvent>.stackTrace				
				
	  And Exit Action