package ##Java Main Package##.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("##JourneyBasePackage##.##JourneyEnterprise##.journey.##JourneyFunctionalDomain##")
public class ##JourneyClassName##JourneyConfig {
}