Feature: Create Journey
  Create Journey
  
  Scenario: Validate Input Data
	Given Initiation of a Journey
	 When <InputEvent>.accountType != NULL AND <InputEvent>.accountType NOT IN value on configuration parameter "core#tutorial#account#input-validation-account-type"
	 Then Create FEEDBACK output event "Account Error" with output data:

				- <OutputEvent>.<HTTPStatusCode> = 400
				- <OutputEvent>.returnCode = "F101"
				- <OutputEvent>.returnMessage = "Invalid value for account type"

	  And Exit Action

  Scenario: Fill Journey Data
    Given Scenario "Validate Input Data" completed
     Then Create new Journey
     
      And Fill journey data with details of input event, i.e.
			
		 <JourneyData>.accountReference = <InputEvent>.accountReference
		 <JourneyData>.accountType = <InputEvent>.accountType
		 <JourneyData>.balanceAmount = <InputEvent>.balanceAmount
		 		 
  Scenario: Create output event
     When Scenario "Fill Journey Data" completed
	 Then Create FEEDBACK output event "Journey Created" with event data:
	 
		- <OutputEvent>.returnCode = "OK"
		- <OutputEvent>.returnMessage = "Account created"
