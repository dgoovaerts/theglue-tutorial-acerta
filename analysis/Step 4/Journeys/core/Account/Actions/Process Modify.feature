Feature: Process Modify
  Process Modify

  Scenario: Validate Input Data
	Given Action started
	 When <InputEvent>.accountType != NULL AND <InputEvent>.accountType NOT IN value on configuration parameter "core#tutorial#account#input-validation-account-type"
	 Then Create FEEDBACK output event "Account Error" with output data:

				- <OutputEvent>.<HTTPStatusCode> = 400
				- <OutputEvent>.returnCode = "F101"
				- <OutputEvent>.returnMessage = "Invalid value for account type"

	  And Exit Action
	  
  Scenario: Process Modification
    Given Scenario "Validate Input Data" completed
     Then Enrich journey data with details of input event, i.e.
			
		 <JourneyData>.accountType = IF <InputEvent>.accountType != NULL THEN <InputEvent>.accountType ELSE no update
		 <JourneyData>.balanceAmount = <InputEvent>.balanceAmount

  Scenario: Create output event
     When Scenario "Process Modification" completed
	 Then Create FEEDBACK output event "Modification Completed" with event data:
	 
		- <OutputEvent>.returnCode = "OK"
		- <OutputEvent>.returnMessage = "Account successfully modified"