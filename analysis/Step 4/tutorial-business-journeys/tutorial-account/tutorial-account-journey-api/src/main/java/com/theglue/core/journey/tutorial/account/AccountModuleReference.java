package com.theglue.core.journey.tutorial.account;

import com.theglue.api.ModuleReference;

public class AccountModuleReference {

    public static final ModuleReference ACCOUNT_JOURNEY = ModuleReference.create("core", "tutorial", "account", "1.0");
}
