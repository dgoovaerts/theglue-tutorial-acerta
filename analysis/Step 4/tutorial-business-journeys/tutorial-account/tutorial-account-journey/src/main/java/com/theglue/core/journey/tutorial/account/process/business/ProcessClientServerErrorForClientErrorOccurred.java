package com.theglue.core.journey.tutorial.account.process.business;

import static com.theglue.api.processing.Result.result;

import javax.inject.Inject;
import javax.inject.Named;

import com.theglue.api.InjectTheGlueContext;
import com.theglue.api.TheGlueContext;
import com.theglue.api.processing.JourneyProcessor;
import com.theglue.api.processing.ProcessingContext;
import com.theglue.api.processing.Result;

import com.theglue.core.journey.tutorial.account.data.journeydata.AccountJourneyData;
import com.theglue.api.events.ClientErrorOccurred;
import com.theglue.core.journey.tutorial.account.events.AccountError;

@Named
public class ProcessClientServerErrorForClientErrorOccurred implements JourneyProcessor<com.theglue.api.events.ClientErrorOccurred, AccountJourneyData> {

    @InjectTheGlueContext
    private TheGlueContext theGlueContext;

    @Override
    public Result process(ProcessingContext<com.theglue.api.events.ClientErrorOccurred, AccountJourneyData> processingContext) {
        AccountJourneyData journeyData = processingContext.getJourneyData();
        ClientErrorOccurred incomingEvent = processingContext.getEvent();

    	/** Introduce code of the action here
    	*/


	return Result.result().postFeedbackEvent(incomingEvent);

    }

    /** Introduce code of the action here
    */
}
