package com.theglue.core.journey.tutorial.account.process.business;

import static com.theglue.api.processing.Result.result;

import javax.inject.Inject;
import javax.inject.Named;

import com.theglue.api.InjectTheGlueContext;
import com.theglue.api.TheGlueContext;
import com.theglue.api.processing.JourneyProcessor;
import com.theglue.api.processing.ProcessingContext;
import com.theglue.api.processing.Result;

import com.theglue.core.journey.tutorial.account.data.journeydata.AccountJourneyData;
import com.theglue.core.journey.tutorial.account.events.CreationRequested;
import com.theglue.core.journey.tutorial.account.events.JourneyCreated;
import com.theglue.core.journey.tutorial.account.events.AccountError;

@Named
public class CreateJourney implements JourneyProcessor<com.theglue.core.journey.tutorial.account.events.CreationRequested, AccountJourneyData> {

    @InjectTheGlueContext
    private TheGlueContext theGlueContext;

    @Override
    public Result process(ProcessingContext<com.theglue.core.journey.tutorial.account.events.CreationRequested, AccountJourneyData> processingContext) {
        AccountJourneyData journeyData = processingContext.getJourneyData();
        CreationRequested incomingEvent = processingContext.getEvent();

    	/** Introduce code of the action here
    	*/


	return Result.result().postFeedbackEvent(incomingEvent);

    }

    /** Introduce code of the action here
    */
}
