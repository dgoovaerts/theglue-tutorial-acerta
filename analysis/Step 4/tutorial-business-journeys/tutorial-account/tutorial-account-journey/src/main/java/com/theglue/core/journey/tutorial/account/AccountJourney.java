package com.theglue.core.journey.tutorial.account;


import javax.inject.Named;

import com.theglue.core.journey.tutorial.account.data.journeydata.AccountJourneyData;



import com.theglue.core.journey.tutorial.account.events.CreationRequested;
import com.theglue.core.journey.tutorial.account.events.ModificationRequested;
import com.theglue.api.events.ClientErrorOccurred;
import com.theglue.api.events.ServerErrorOccurred;


import com.theglue.core.journey.tutorial.account.process.business.CreateJourney;
import com.theglue.core.journey.tutorial.account.process.business.ProcessModify;
import com.theglue.core.journey.tutorial.account.process.business.ProcessClientServerError;


import com.theglue.api.processing.builders.business.BusinessJourney;
import com.theglue.api.processing.builders.business.BusinessJourneyBuilder;

@Named
public class AccountJourney implements BusinessJourney {

    @Override
    public void configure(BusinessJourneyBuilder journey) {
        journey
                 .withData(AccountJourneyDataDefinition.class)
                 .event(CreationRequested.class).isInitiating().action(CreateJourney.class).end().end()
                 .event(ModificationRequested.class).action(ProcessModify.class).end().end()
                 .event(ClientErrorOccurred.class).action(ProcessClientServerErrorForClientErrorOccurred.class).end().end()
                 .event(ServerErrorOccurred.class).action(ProcessClientServerErrorForServerErrorOccurred.class).end().end()
;
    }
}
