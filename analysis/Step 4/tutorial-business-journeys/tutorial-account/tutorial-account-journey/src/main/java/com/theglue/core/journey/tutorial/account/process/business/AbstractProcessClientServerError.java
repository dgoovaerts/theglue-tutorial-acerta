package com.theglue.core.journey.tutorial.account.process.business;

import javax.inject.Named;

import com.theglue.api.processing.JourneyProcessor;
import com.theglue.api.processing.ProcessingContext;
import com.theglue.api.processing.Result;

import com.theglue.core.journey.tutorial.account.data.journeydata.AccountJourneyData;
import com.theglue.core.journey.tutorial.account.events.AccountError;

@Named
abstract class AbstractProcessClientServerError {

	@Override

	/** Introduce code of the action here
	*/
}
