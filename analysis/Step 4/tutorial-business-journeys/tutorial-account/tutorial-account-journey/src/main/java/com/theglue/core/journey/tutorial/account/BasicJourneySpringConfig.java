package com.theglue.core.journey.tutorial.account;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.theglue.core.journey.tutorial.account", "com.theglue.core.entity.tutorial.account"})
public class BasicJourneySpringConfig {

}
