package com.theglue.core.journey.tutorial.account;

import javax.inject.Named;

import com.theglue.api.journeydata.JourneyDataDefinition;
import com.theglue.api.journeydata.builders.JourneyDataBuilder;
import com.theglue.core.journey.tutorial.account.data.journeydata.AccountJourneyData;
import com.theglue.core.journey.tutorial.account.data.journeydata.AccountJourneyData.query.accountjourneydata.AccountJourneyDataQueryObject;

@Named
public class AccountJourneyDataDefinition implements JourneyDataDefinition<AccountJourneyData> {

    @Override
    public void define(JourneyDataBuilder<AccountJourneyData> journeyDataBuilder) {
        journeyDataBuilder
            .journeyDataOfType(AccountJourneyData.class)
            .queryObject(AccountJourneyDataQueryObject.class);
    }

}
