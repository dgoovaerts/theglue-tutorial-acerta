The Glue tutorial implementation and analysis
-------------------------

This repository contains all implementation and analysis artifacts for The Glue tutorial.

Structure of the repository
-------------------------

This repository contains several branches. For each different step of the tutorial, a branch exists containing exactly the code that implements that step. The analysis artifacts can be found in both master and develop branches.

To summarize:

**Implementation branches**

* step0 - Contains a separate README with the steps needed to get started
* step1
* step2
* step3
* step4
* step5
* step6
* step7

**Analysis branches**

* develop (in folder _analysis_)
* master (in folder _analysis_)